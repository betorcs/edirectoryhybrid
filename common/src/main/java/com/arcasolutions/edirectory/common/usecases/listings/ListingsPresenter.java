package com.arcasolutions.edirectory.common.usecases.listings;

import com.arcasolutions.edirectory.common.client.WebService;
import com.arcasolutions.edirectory.common.model.ListingResult;
import com.arcasolutions.edirectory.common.model.Paging;

import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ListingsPresenter implements ListingsContract.Presenter {

    private final ListingsContract.View mView;
    private final WebService mWebService;
    private final Scheduler mObserveOn;
    private Disposable mDisposable;

    public ListingsPresenter(ListingsContract.View view, Scheduler observeOn, WebService webService) {
        mView = view;
        mWebService = webService;
        mObserveOn = observeOn;
    }

    @Override
    public void loadListings(int page) {
        mView.showProgress();
        mDisposable = mWebService.getListings(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(mObserveOn)
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        mView.hideProgress();
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.hideProgress();
                    }
                })
                .doOnNext(new Consumer<ListingResult>() {
                    @Override
                    public void accept(@NonNull ListingResult listingResult) throws Exception {
                        Paging paging = listingResult.getPaging();
                        mView.setPaging(paging.getPage(), paging.getPages() > paging.getPage());
                        mView.setListings(listingResult.getData());
                    }
                })
                .subscribe();
    }

    @Override
    public void unbind() {
        if (mDisposable != null)
            mDisposable.dispose();
    }
}
