package com.arcasolutions.edirectory.common.model;

import java.util.Locale;

public class Geo {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "%.07f,%.07f", lat, lng);
    }
}
