package com.arcasolutions.edirectory.common.client;

import com.arcasolutions.edirectory.common.model.ListingResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebService {

    @GET("results.json?token=16200&module=listing")
    Observable<ListingResult> getListings(@Query("page") int page);

}
