package com.arcasolutions.edirectory.common.model;


import java.util.List;

public class ListingResult {

    private Paging paging;
    private List<Listing> data;

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public List<Listing> getData() {
        return data;
    }

    public void setData(List<Listing> data) {
        this.data = data;
    }
}
