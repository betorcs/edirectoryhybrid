package com.arcasolutions.edirectory.common.usecases.listings;

import com.arcasolutions.edirectory.common.model.Listing;

import java.util.List;

public interface ListingsContract {

    public interface View {
        void showProgress();
        void hideProgress();
        void setListings(List<Listing> listings);
        void setPaging(int page, boolean hasMoreListings);
    }

    public interface Presenter {
        void loadListings(int page);
        void unbind();
    }

}
