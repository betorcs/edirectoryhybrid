package com.arcasolutions.edirectory.platform;

import com.arcasolutions.edirectory.common.model.Geo;

public interface Platform {

    Geo getLocation();

}
