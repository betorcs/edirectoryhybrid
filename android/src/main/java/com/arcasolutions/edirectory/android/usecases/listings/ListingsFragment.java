package com.arcasolutions.edirectory.android.usecases.listings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import com.arcasolutions.edirectory.android.R;
import com.arcasolutions.edirectory.common.WebServiceFactory;
import com.arcasolutions.edirectory.common.usecases.listings.ListingsContract;
import com.arcasolutions.edirectory.common.model.Listing;
import com.arcasolutions.edirectory.common.usecases.listings.ListingsPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class ListingsFragment extends Fragment implements ListingsContract.View {

    private ListingsPresenter mPresenter;
    private SimpleAdapter mAdapter;
    private final List<Map<String, String>> mAdapterData = new ArrayList<>();
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new ListingsPresenter(this, AndroidSchedulers.mainThread(), WebServiceFactory.getService());
        String[] from = new String[]{"title", "address"};
        int[] to = new int[]{R.id.title, R.id.address};
        mAdapter = new SimpleAdapter(getContext(), mAdapterData, R.layout.listing_item_view, from, to);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listings, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        ListView listView = (ListView) view.findViewById(R.id.listView);
        listView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.loadListings(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unbind();
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void setListings(List<Listing> listings) {
        mAdapterData.clear();

        Map<String, String> item;
        for (Listing listing : listings) {
            item = new HashMap<>();
            item.put("title", listing.getTitle());
            item.put("address", listing.getAddress());
            mAdapterData.add(item);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPaging(int page, boolean hasMoreListings) {

    }
}
