package com.arcasolutions.edirectory.ios.usecases.listings;

import com.arcasolutions.edirectory.common.model.Listing;
import com.arcasolutions.edirectory.ios.view.DownloadImageView;

import org.moe.natj.general.Pointer;
import org.moe.natj.general.ann.RegisterOnStartup;
import org.moe.natj.objc.ObjCRuntime;
import org.moe.natj.objc.ann.IBOutlet;
import org.moe.natj.objc.ann.ObjCClassName;
import org.moe.natj.objc.ann.Property;
import org.moe.natj.objc.ann.Selector;

import apple.uikit.UILabel;
import apple.uikit.UITableViewCell;

@org.moe.natj.general.ann.Runtime(ObjCRuntime.class)
@ObjCClassName("ListingTableViewCell")
@RegisterOnStartup
public class ListingTableViewCell extends UITableViewCell {

    @Selector("titleLabel")
    @Property
    @IBOutlet
    private native UILabel getTitleLabel();

    @Selector("addressLabel")
    @Property
    @IBOutlet
    private native UILabel getAddressLabel();

    @Selector("imageView")
    @Property
    @IBOutlet
    private native DownloadImageView getImageView();

    protected ListingTableViewCell(Pointer peer) {
        super(peer);
    }

    void setListing(Listing listing) {
        getTitleLabel().setText(listing.getTitle());
        getAddressLabel().setText(listing.getAddress());
        getImageView().setUrl(listing.getImageUrl());
    }

    @Override
    public void prepareForReuse() {
        super.prepareForReuse();
        getImageView().dispose();
    }
}
