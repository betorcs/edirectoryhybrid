package com.arcasolutions.edirectory.ios.rx;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

import apple.foundation.NSOperationQueue;
import io.reactivex.Scheduler;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;


public class iOSSchedulers {

    private static final Scheduler MAIN_THREAD = RxJavaPlugins.initComputationScheduler(new Callable<Scheduler>() {

        NSOperationQueue mainQueue = NSOperationQueue.mainQueue();

        @Override
        public Scheduler call() throws Exception {
            return new Scheduler() {
                @Override
                public Worker createWorker() {
                    return new ExecutorScheduler.ExecutorWorker(new Executor() {
                        @Override
                        public void execute(Runnable command) {
                            mainQueue.addOperationWithBlock(new NSOperationQueue.Block_addOperationWithBlock() {
                                @Override
                                public void call_addOperationWithBlock() {
                                    command.run();
                                }
                            });
                        }
                    });
                }
            };
        }
    });

    public static Scheduler mainThread() {
        return RxJavaPlugins.onComputationScheduler(MAIN_THREAD);
    }

}
