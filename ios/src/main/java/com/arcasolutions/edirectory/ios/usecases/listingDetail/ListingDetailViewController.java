package com.arcasolutions.edirectory.ios.usecases.listingDetail;

import com.arcasolutions.Main;
import com.arcasolutions.edirectory.common.model.Listing;
import com.arcasolutions.edirectory.ios.view.DownloadImageView;

import org.moe.natj.general.Pointer;
import org.moe.natj.general.ann.RegisterOnStartup;
import org.moe.natj.objc.ObjCRuntime;
import org.moe.natj.objc.ann.IBOutlet;
import org.moe.natj.objc.ann.ObjCClassName;
import org.moe.natj.objc.ann.Property;
import org.moe.natj.objc.ann.Selector;

import apple.corelocation.struct.CLLocationCoordinate2D;
import apple.mapkit.MKMapView;
import apple.mapkit.MKPointAnnotation;
import apple.mapkit.struct.MKCoordinateRegion;
import apple.mapkit.struct.MKCoordinateSpan;
import apple.uikit.UIApplication;
import apple.uikit.UILabel;
import apple.uikit.UIViewController;

/**
 * Created by luizfernandosalvaterra on 28/06/17.
 */

@org.moe.natj.general.ann.Runtime(ObjCRuntime.class)
@ObjCClassName("ListingDetailViewController")
@RegisterOnStartup
public class ListingDetailViewController extends UIViewController {

    private Listing mListing;

    private MKPointAnnotation pointAnnotation;

    @Selector("listingImageView")
    @Property
    @IBOutlet
    private native DownloadImageView getListingImageView();

    @Selector("listingMap")
    @Property
    @IBOutlet
    private native MKMapView getListingMap();

    @Selector("listingTitle")
    @Property
    @IBOutlet
    private native UILabel getListingLabelTitle();

    @Selector("listingAddress")
    @Property
    @IBOutlet
    private native UILabel getListingLabelAddress();

    protected ListingDetailViewController(Pointer peer) {
        super(peer);
    }

    @Override
    public void viewDidLoad() {
        super.viewDidLoad();

        if (mListing != null) {
            getListingImageView().setUrl(mListing.getImageUrl());
            getListingLabelTitle().setText(mListing.getTitle());

            Main main = (Main) UIApplication.sharedApplication().delegate();

            getListingLabelAddress().setText(mListing.getAddress() + "\n"
                    + main.getPlatform().getLocation().toString());


            CLLocationCoordinate2D location = new CLLocationCoordinate2D();
            location.setLatitude(mListing.getGeo().getLat());
            location.setLongitude(mListing.getGeo().getLng());

            MKCoordinateSpan span  = new MKCoordinateSpan();
            span.setLatitudeDelta(0.01);
            span.setLongitudeDelta(0.01);

            MKCoordinateRegion region = new MKCoordinateRegion(location, span);

            pointAnnotation = MKPointAnnotation.alloc().init();

            pointAnnotation.setCoordinate(region.center());
            pointAnnotation.setTitle(mListing.getTitle());

            getListingMap().addAnnotation(pointAnnotation);
            getListingMap().setRegion(region);



        }
    }

    public void setListing(Listing listing) {
        mListing = listing;
    }
}
