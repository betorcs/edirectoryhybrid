package com.arcasolutions.edirectory.ios.usecases.listings;

import com.arcasolutions.edirectory.common.WebServiceFactory;
import com.arcasolutions.edirectory.common.model.Listing;
import com.arcasolutions.edirectory.common.usecases.listings.ListingsContract;
import com.arcasolutions.edirectory.common.usecases.listings.ListingsPresenter;
import com.arcasolutions.edirectory.ios.rx.iOSSchedulers;
import com.arcasolutions.edirectory.ios.usecases.listingDetail.ListingDetailViewController;

import org.moe.natj.general.Pointer;
import org.moe.natj.general.ann.Mapped;
import org.moe.natj.general.ann.NInt;
import org.moe.natj.general.ann.Owned;
import org.moe.natj.general.ann.RegisterOnStartup;
import org.moe.natj.objc.ObjCRuntime;
import org.moe.natj.objc.ann.IBOutlet;
import org.moe.natj.objc.ann.ObjCClassName;
import org.moe.natj.objc.ann.Property;
import org.moe.natj.objc.ann.Selector;
import org.moe.natj.objc.map.ObjCObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import apple.foundation.NSIndexPath;
import apple.uikit.UIActivityIndicatorView;
import apple.uikit.UIStoryboardSegue;
import apple.uikit.UITableView;
import apple.uikit.UITableViewCell;
import apple.uikit.UIViewController;
import apple.uikit.protocol.UITableViewDataSource;
import apple.uikit.protocol.UITableViewDelegate;

@org.moe.natj.general.ann.Runtime(ObjCRuntime.class)
@ObjCClassName("ListingsViewController")
@RegisterOnStartup
public class ListingsViewController extends UIViewController implements UITableViewDataSource, UITableViewDelegate, ListingsContract.View {

    private ListingsPresenter mPresenter;
    private Listing selectedListing;
    private final List<Listing> mListings = new ArrayList<>();

    @Selector("listingsTableView")
    @Property
    @IBOutlet
    private native UITableView getListingsTableView();

    @Selector("progressView")
    @Property
    @IBOutlet
    private native UIActivityIndicatorView getProgressView();

    @Owned
    @Selector("alloc")
    public static native ListingsViewController alloc();

    @Selector("init")
    public native ListingsViewController init();

    protected ListingsViewController(Pointer peer) {
        super(peer);
    }

    @Override
    public void viewDidLoad() {
        super.viewDidLoad();

        mPresenter = new ListingsPresenter(this, iOSSchedulers.mainThread(), WebServiceFactory.getService());
        mPresenter.loadListings(1);
    }

    @Override
    public void viewWillDisappear(boolean animated) {
        super.viewWillDisappear(animated);
        mPresenter.unbind();
    }

    /* implements UITableViewDataSource */

    @Override
    public UITableViewCell tableViewCellForRowAtIndexPath(UITableView tableView, NSIndexPath indexPath) {
        Listing listing = mListings.get((int) indexPath.row());
        ListingTableViewCell listingTableViewCell = (ListingTableViewCell) tableView.dequeueReusableCellWithIdentifier("listingTableViewCell");
        listingTableViewCell.setListing(listing);
        return listingTableViewCell;
    }

    @Override
    public long tableViewNumberOfRowsInSection(UITableView tableView, @NInt long section) {
        return mListings.size();
    }

    /* implements UITableViewDelegate */

    @Override
    public void tableViewDidSelectRowAtIndexPath(UITableView tableView, NSIndexPath indexPath) {
        selectedListing = mListings.get((int) indexPath.row());
        performSegueWithIdentifierSender("showDetail", this);
    }

    @Override
    public void prepareForSegueSender(UIStoryboardSegue segue, @Mapped(ObjCObjectMapper.class) Object sender) {
        if (Objects.equals(segue.identifier(), "showDetail")) {
            ListingDetailViewController vc = (ListingDetailViewController) segue.destinationViewController();
            vc.setListing(selectedListing);
        }
    }

    /* implements ListingsContract.View */

    @Override
    public void showProgress() {
        getProgressView().setHidden(false);
        getProgressView().startAnimating();
    }

    @Override
    public void hideProgress() {
        getProgressView().stopAnimating();
        getProgressView().setHidden(true);
    }

    @Override
    public void setListings(List<Listing> listings) {
        mListings.clear();
        mListings.addAll(listings);
        getListingsTableView().reloadData();
    }

    @Override
    public void setPaging(int page, boolean hasMoreListings) {

    }
}
