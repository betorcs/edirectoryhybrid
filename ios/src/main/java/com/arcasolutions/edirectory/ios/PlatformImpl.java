package com.arcasolutions.edirectory.ios;

import com.arcasolutions.edirectory.common.model.Geo;
import com.arcasolutions.edirectory.platform.Platform;

import apple.corelocation.CLLocation;
import apple.corelocation.protocol.CLLocationManagerDelegate;

public class PlatformImpl implements Platform, CLLocationManagerDelegate {

    private Geo geo;

    public PlatformImpl(CLLocation lastLocation) {
        geo = new Geo();
        geo.setLng(lastLocation.coordinate().longitude());
        geo.setLat(lastLocation.coordinate().latitude());
    }

    @Override
    public Geo getLocation() {
        return geo;
    }

}
