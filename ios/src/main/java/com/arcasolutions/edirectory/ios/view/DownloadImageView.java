package com.arcasolutions.edirectory.ios.view;

import org.moe.natj.general.Pointer;
import org.moe.natj.general.ann.RegisterOnStartup;
import org.moe.natj.objc.ObjCRuntime;
import org.moe.natj.objc.ann.ObjCClassName;

import apple.foundation.NSData;
import apple.foundation.NSOperationQueue;
import apple.foundation.NSURL;
import apple.uikit.UIImage;
import apple.uikit.UIImageView;


@org.moe.natj.general.ann.Runtime(ObjCRuntime.class)
@ObjCClassName("DownloadImageView")
@RegisterOnStartup
public class DownloadImageView extends UIImageView {

    private interface Callback<T> {
        void done(T o);
    }

    private static final NSOperationQueue otherQueue = NSOperationQueue.alloc().init();
    private static final NSOperationQueue mainQueue = NSOperationQueue.mainQueue();

    protected DownloadImageView(Pointer peer) {
        super(peer);
    }

    private void downloadImageData(String imageUrl, Callback<NSData> callback) {
        otherQueue.addOperationWithBlock(() -> {
            NSData data = NSData.dataWithContentsOfURL(NSURL.URLWithString(imageUrl));
            if (data != null) callback.done(data);
        });
    }

    private void setImageData(NSData data) {
        mainQueue.addOperationWithBlock(() -> setImage(UIImage.imageWithData(data)));
    }

    public void setUrl(String url) {
        downloadImageData(url, this::setImageData);
    }

    public void dispose() {
        otherQueue.cancelAllOperations();
        setImage(null);
    }

}
