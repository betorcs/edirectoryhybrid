package com.arcasolutions;

import com.arcasolutions.edirectory.ios.PlatformImpl;
import com.arcasolutions.edirectory.platform.Platform;

import org.moe.natj.general.Pointer;
import org.moe.natj.general.ann.RegisterOnStartup;
import org.moe.natj.objc.ann.Selector;

import apple.NSObject;
import apple.corelocation.CLLocation;
import apple.corelocation.CLLocationManager;
import apple.corelocation.protocol.CLLocationManagerDelegate;
import apple.foundation.NSArray;
import apple.foundation.NSDictionary;
import apple.struct.Float32Point;
import apple.uikit.UIApplication;
import apple.uikit.UIDevice;
import apple.uikit.UIWindow;
import apple.uikit.c.UIKit;
import apple.uikit.protocol.UIApplicationDelegate;

@RegisterOnStartup
public class Main extends NSObject implements UIApplicationDelegate, CLLocationManagerDelegate {

    private CLLocationManager mLocationManager;
    private CLLocation mLastLocation;

    public static void main(String[] args) {
        UIKit.UIApplicationMain(0, null, null, Main.class.getName());
    }

    @Selector("alloc")
    public static native Main alloc();

    protected Main(Pointer peer) {
        super(peer);

        mLocationManager = CLLocationManager.alloc().init();
    }

    private UIWindow window;

    @Override
    public boolean applicationDidFinishLaunchingWithOptions(UIApplication application, NSDictionary launchOptions) {

        mLocationManager = CLLocationManager.alloc().init();
        mLocationManager.setDelegate(this);
        mLastLocation = mLocationManager.location();

        String systemVersion = UIDevice.currentDevice().systemVersion();
        int versionNumber = Integer.parseInt(systemVersion.substring(0, systemVersion.indexOf('.')));
        if (versionNumber > 8) {
            mLocationManager.requestWhenInUseAuthorization();
        }

        mLocationManager.startUpdatingLocation();
        return true;
    }

    @Override
    public boolean applicationWillFinishLaunchingWithOptions(UIApplication application, NSDictionary<?, ?> launchOptions) {
        mLocationManager.stopUpdatingLocation();
        return true;
    }

    @Override
    public void setWindow(UIWindow value) {
        window = value;
    }

    @Override
    public UIWindow window() {
        return window;
    }

    @Override
    public void locationManagerDidUpdateLocations(CLLocationManager manager, NSArray<? extends CLLocation> locations) {
        mLastLocation = locations.lastObject();
    }

    public CLLocation getLastLocation() {
        return mLastLocation;
    }

    public Platform getPlatform() {
        return new PlatformImpl(getLastLocation());
    }
}
